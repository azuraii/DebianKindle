#!/bin/bash

echo "[*] Making Debian image for Kindle..."
echo "[*] Creating Image File (THIS WILL TAKE A WHILE)..."
dd if=/dev/zero of=debian.ext3 bs=1M count=1280
echo "[*] Making ext3 Filesystem..."
mkfs.ext3 debian.ext3
tune2fs -i 0 -c 0 debian.ext3
echo "[*] Mounting Filesystem..."
mkdir /mnt/debian
sudo mount -o loop -t ext3 debian.ext3 /mnt/debian
echo "[*] Installing Debian using debootstrap (THIS WILL TAKE A WHILE)..."
 
sudo debootstrap --arch armel --foreign wheezy /mnt/debian http://archive.debian.org/debian-archive/debian/
 
echo "[*] Unmounting image and removing mountpoint..."
sudo umount /mnt/debian
sudo rm -r /mnt/debian
echo "[*] Image generation complete, you can now copy debian.ext3 to the Kindle..."